#!/usr/bin/env bash
#
# Generate a TLS key and certificate.
#
# Based on Onionmine's generate-selected-cert:
# https://gitlab.torproject.org/tpo/onion-services/onionmine/-/blob/main/bin/generate-selected-cert
#
# Based on Onionsprays's make-selfsigned-wildcard-ssl-cert.sh:
# https://gitlab.torproject.org/tpo/onion-services/onionspray/-/blob/main/lib/make-selfsigned-wildcard-ssl-cert.sh
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Parameters
BASENAME="`basename $0`"
DIRNAME="`dirname $0`"

# Additional parameters
OUTPUT="/etc/nginx/certs.d"
ONIONADDR="$1"
KEY="$OUTPUT/$ONIONADDR.pem"
#CSR="$OUTPUT/$ONIONADDR.csr"
CERT="$OUTPUT/$ONIONADDR.crt"
#CONF="$OUTPUT/$ONIONADDR.conf"
#PASS="$OUTPUT/$ONIONADDR.pass"
#PASSWORD_SIZE="`shuf -i 20-50 -n 1`"
DAYS="365"

function usage() {
  echo "usage: $BASENAME <onionaddr>"
  echo ""
  echo "Generate a TLS key and certiicate for HTTPS connections for the given Onion Service address"
  echo ""
  echo "    onionaddr: the .onion address"
}

# Check usage
if [ -z "$ONIONADDR" ]; then
  usage
  exit 1
fi

# Check if OpenSSL is available
if ! which openssl &> /dev/null; then
  echo "$BASENAME: please install OpenSSL"
  exit 1
fi

# Check for an existing private key
#if [ -e "$KEY" ]; then
#  echo "$BASENAME: key file $KEY already exists"
#  exit 1
#fi

# Ensure that the output folder exists
#mkdir -p -m 700 $OUTPUT || exit 1
if [ ! -d "$OUTPUT" ]; then
  echo "$BASENAME: folder not found: $OUTPUT"
  exit 1
fi

# Previous approach, using mkcert
#apt update && apt install mkcert -y
#onionaddr="someonionserviceaddress27xfospjoatjcwnuztukyv43uaqpylzyd.onion"
#mkcert --cert-file /etc/nginx/certs.d/${onionaddr}.crt \
#        --key-file /etc/nginx/certs.d/${onionaddr}.pem \
#        "*.${onionaddr}"
#chown -R nobody /etc/nginx/certs.d

# Create an OpenSSL configuration
#cat <<EOF >> $CONF
#[ req ]
#distinguished_name = req_distinguished_name
#
#[ req_distinguished_name ]
#commonName_default     = $ONIONADDR
#organizationName       = .
#organizationalUnitName = .
#emailAddress           = .
#localityName           = .
#stateOrProvinceName    = .
#countryName            = .
#commonName             = .
#subjectAltName         = DNS:*.${ONIONADDR}
#EOF

# Generate a passphrase
#touch     $PASS
#chmod 600 $PASS || exit 1
#head -c $PASSWORD_SIZE /dev/urandom | base64 > $PASS

# Generate the key
openssl ecparam         \
        -genkey         \
        -out $KEY       \
        -name secp384r1

# Generate a self-signed certificate
openssl req                                                          \
        -days $DAYS                                                  \
        -nodes                                                       \
        -x509                                                        \
        -new                                                         \
        -key $KEY                                                    \
        -subj "/CN=$ONIONADDR"                                       \
        -addext "subjectAltName=DNS:${ONIONADDR},DNS:*.${ONIONADDR}" \
        -out $CERT                                                   \
        -sha384                                                     #\
        #-config $CONF \

# Set ownership
chown -R nobody $OUTPUT

# Teardown
rm -f $CONF
exit $?
