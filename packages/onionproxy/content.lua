-- Onionproxy: a rewriting HTTP proxy for Onion Services.
--
-- This file holds the HTTP response rewriter logic.
--
-- Copyright (C) 2024 The Tor Project, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published
-- by the Free Software Foundation, either version 3 of the License,
-- or any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Make a subrequest to the proxy endpoint
method     = methods[ngx.var.request_method]
local args = ngx.req.get_uri_args()
local res  = ngx.location.capture('/_onionproxy' .. ngx.var.uri .. '?' .. ngx.encode_args(args), {
  method        = method,
  body          = ngx.var.request_body,
  copy_all_vars = true,
})
ngx.status = res.status

-- Set the response headers
for k,v in pairs(res.header) do
  ngx.header[k] = v
end

-- Make body available for asynchronously processing here
local body = res.body

-- Apply substitutions
body = ApplyMap(body)

-- Set status
--ngx.status = 429

-- Print the body
ngx.print(body)

-- Return
--return ngx.exit(ngx.HTTP_OK)
return ngx.exit(res.status)
