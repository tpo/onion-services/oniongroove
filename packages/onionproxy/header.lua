-- Onionproxy: a rewriting HTTP proxy for Onion Services.
--
-- This file holds the HTTP header rewriter logic.
--
-- Copyright (C) 2024 The Tor Project, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published
-- by the Free Software Foundation, either version 3 of the License,
-- or any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Ensure to reset the Content-Length header, or the client might misbehave
ngx.header.content_length = nil

local response_rewrites = {
  "Content-Security-Policy",
  "Content-Security-Policy-Report-Only",
  "Access-Control-Allow-Origin",
  "Content-Location",
  "Feature-Policy",
  "Link",
  "Location",
  "Set-Cookie",
  "Timing-Allow-Origin"
}

-- Apply substitutions
for m,n in pairs(response_rewrites) do
  if ngx.header[n] then
    ngx.header[n] = ApplyMap(ngx.header[n])
  end
end

-- Location workaround during experimentation when HTTPS mode is not supported
--if ngx.header['Location'] then
--  ngx.header['Location'] = ngx.re.gsub(ngx.header['Location'], 'https://', 'http://')
--end
