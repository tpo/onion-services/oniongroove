-- Onionproxy: a rewriting HTTP proxy for Onion Services.
--
-- This file holds TLS handshake logic.
--
-- Copyright (C) 2024 The Tor Project, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published
-- by the Free Software Foundation, either version 3 of the License,
-- or any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Load SSL methods and get the SNI
local ssl         = require "ngx.ssl"
local sni_name    = ssl.server_name()

-- Paths
local ssl_path = '/etc/nginx/certs.d/'
local gencert  = '/usr/local/sbin/generate-certificate'

-- Parse the SNI domain
local matches, err = ngx.re.match(sni_name, "^(?<subdomains>([-0-9a-z]+\\.)*)(?<onionaddr>[-0-9-a-z]{56}\\.onion)$")
if err then
  ngx.log(ngx.ERR, "could not determine the base .onion domain: ", err)
   return ngx.exit(ngx.ERROR)
end

-- Determine the base .onion domain
domain = matches['onionaddr']

-- Debug
--ngx.log(ngx.STDERR, 'SNI: ', sni_name)
--ngx.log(ngx.STDERR, 'Domain: ', domain)

-- Ensure a certificate exists
local cert_file = ssl_path .. domain .. '.crt'
if not file_exists(cert_file) then
  -- Generate a self-signed certificate on the fly
  --ngx.log(ngx.STDERR, 'Generating a self-signed certificate for: ', domain)
  local handle = io.popen(gencert .. ' ' .. domain)
  handle:close()
end

-- Load the certificate
local pem, err = readall(cert_file)
if pem ~= nil then
  cert = ssl.parse_pem_cert(pem)
else
  ngx.log(ngx.ERR, "could not read certificate file (err: " .. err .. "): " .. cert_file)
  return ngx.exit(ngx.ERROR)
end

-- Load the private key
local priv_file = ssl_path .. domain .. '.pem'
local pem, err  = readall(priv_file)
if pem ~= nil then
  key = ssl.parse_pem_priv_key(pem)
else
  ngx.log(ngx.ERR, "could not read private key file (err: " .. err .. "): " .. cert_file)
  return ngx.exit(ngx.ERROR)
end

-- Clear the fallback certificates and private keys set by the
-- ssl_certificate and ssl_certificate_key directives above.
local ok, err = ssl.clear_certs()
if not ok then
  ngx.log(ngx.ERR, "failed to clear existing (fallback) certificates")
  return ngx.exit(ngx.ERROR)
end

-- Set the proper key
local ok, err = ssl.set_cert(cert)
if not ok then
  ngx.log(ngx.ERR, "failed to set certificate for " .. domain)
  return ngx.exit(ngx.ERROR)
end

-- Set the proper certificate
local ok, err = ssl.set_priv_key(key)
if not ok then
  ngx.log(ngx.ERR, "failed to set the private key for " .. domain)
  return ngx.exit(ngx.ERROR)
end
