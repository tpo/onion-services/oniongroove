-- Onionproxy: a rewriting HTTP proxy for Onion Services.
--
-- This file holds initialization procedures.
--
-- Copyright (C) 2024 The Tor Project, Inc.
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published
-- by the Free Software Foundation, either version 3 of the License,
-- or any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Lookup table for supported HTTP methods
methods = {
  GET       = ngx.HTTP_GET;
  HEAD      = ngx.HTTP_HEAD,
  PUT       = ngx.HTTP_PUT,
  POST      = ngx.HTTP_POST,
  DELETE    = ngx.HTTP_DELETE,
  OPTIONS   = ngx.HTTP_OPTIONS,
  MKCOL     = ngx.HTTP_MKCOL,
  COPY      = ngx.HTTP_COPY,
  MOVE      = ngx.HTTP_MOVE,
  PROPFIND  = ngx.HTTP_PROPFIND,
  PROPPATCH = ngx.HTTP_PROPPATCH,
  LOCK      = ngx.HTTP_LOCK,
  UNLOCK    = ngx.HTTP_UNLOCK,
  PATCH     = ngx.HTTP_PATCH,
  TRACE     = ngx.HTTP_TRACE,
}

-- Check if a file exists
-- Return true if it exists and is readable.
-- Borrowed from http://lua-users.org/wiki/FileInputOutput
function file_exists(path)
  local file = io.open(path, "rb")
  if file then file:close() end
  return file ~= nil
end

-- Read contents from a file
-- Borrowed from http://lua-users.org/wiki/FileInputOutput
function readall(filename)
  local fh       = assert(io.open(filename, "rb"))
  local contents = assert(fh:read(_VERSION <= "Lua 5.2" and "*a" or "a"))

  fh:close()

  return contents
end

local cjson  = require "cjson"
local config = '/etc/nginx/data.d/onionproxy.json'

-- Load the mapping table
if file_exists(config) then
  json_mappings = readall(config)

  if json_mappings ~= nil then
    mappings = cjson.decode(json_mappings)
  else
    ngx.log(ngx.ERR, "could not load " .. config)
    return ngx.exit(ngx.ERROR)
  end
else
  ngx.log(ngx.ERR, "file not found: " .. config)
  return ngx.exit(ngx.ERROR)
end

-- Basic onionsite proxy mappings for testing
--mappings = {
--  ['torproject.org'] = 'ynceiqaxvn4ckdqc57eq7xs27xfospjoatjcwnuztukyv43uaqpylzyd.onion';
--}

-- To hold the onionsite mappings in other formats
regex_mappings         = {}
reverse_mappings       = {}
reverse_regex_mappings = {}

-- Populate the regex mappings
for k,v in pairs(mappings) do
  regex_mappings["(\\b)" .. ngx.re.gsub(k, "\\.", "\\.", 'o') .. "\\b"]         = "${1}" .. v
  reverse_regex_mappings["(\\b)" .. ngx.re.gsub(v, "\\.", "\\.", 'o') .. "\\b"] = "${1}" .. k

  reverse_mappings[v] = k
end

ApplyMap = function(i)
  for k,v in pairs(regex_mappings) do
    i = ngx.re.gsub(i, k, v)
  end

  return i
end

ApplyReverseMap = function(i)
  for k,v in pairs(reverse_regex_mappings) do
    i = ngx.re.gsub(i, k, v)
  end

  return i
end
