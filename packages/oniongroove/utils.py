#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Oniongroove.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import base64
import hashlib
import re

from typing import Optional, Sequence, Union

# Hidden service addresses are sixteen or fifty six base32 characters.
# Borrowed from Stem: https://stem.torproject.org/api/util/tor_tools.html
HS_V2_ADDRESS_PATTERN = re.compile('^[a-z2-7]{16}$')
HS_V3_ADDRESS_PATTERN = re.compile('^[a-z2-7]{56}$')

class OniongrooveUtils:
    """
    Oniongroove class with utility methods.
    """

    def get_pubkey_from_address(self, address):
        """
        Extract .onion pubkey from the address

        Leaves out the .onion domain suffix and any existing subdomains.

        Borrowed from Onionprobe:
        https://gitlab.torproject.org/tpo/onion-services/onionprobe/-/blob/main/packages/onionprobe/descriptor.py

        :type  address: str
        :param address: Onion Service address

        :rtype:  str
        :return: Onion Service public key
        """

        # Extract
        pubkey = address[0:-6].split('.')[-1]

        return pubkey

    def _to_unicode(self, msg: Union[str, bytes]) -> str:
        """
        Provides the unicode string for the given ASCII bytes. This is purely to
        provide python 3 compatability, normalizing the unicode/ASCII change in the
        version bump.

        Borrowed and adapted from Stem:
        https://stem.torproject.org/api/util/str_tools.html

        :type  msg: str
        :param msg: String to be converted

        :rtype:  str
        :return: unicode conversion
        """

        if msg is not None and not isinstance(msg, str):
            return msg.decode('utf-8', 'replace')
        else:
            return msg

    def is_valid_hidden_service_address(self, entry: str, version: Optional[Union[int, Sequence[int]]] = None) -> bool:
        """
        Checks if a string is a valid format for being a hidden service address
        (not including the '.onion' suffix).

        Borrowed and adapted from Stem:
        https://stem.torproject.org/_modules/stem/util/tor_tools.html

        :type  entry: str
        :param entry: String to check

        :type  version: int or tuple
        :param version: Versions to check for, if unspecified either v2 or v3
                        hidden service address will provide True

        :returns: True if the string could be a hidden service address, False otherwise
        """

        if isinstance(entry, bytes):
            entry = self._to_unicode(entry)

        if version is None:
            version = (2, 3)
        elif isinstance(version, int):
            version = [version]
        elif not isinstance(version, (list, tuple)):
            raise ValueError('Hidden service version must be an integer or list, not a %s' % type(version).__name__)

        try:
            if 2 in version and bool(HS_V2_ADDRESS_PATTERN.match(entry)):
                return True

            if 3 in version and bool(HS_V3_ADDRESS_PATTERN.match(entry)):
                # onion_address = base32(PUBKEY | CHECKSUM | VERSION) + ".onion"

                decoded                        = base64.b32decode(entry.upper())
                pubkey, checksum, addr_version = decoded[:32], decoded[32:34], decoded[34:]

                if addr_version != b'\x03':
                    return False # VERSION component must be three

                # CHECKSUM = H(".onion checksum" | PUBKEY | VERSION)[:2]
                expected_checksum = hashlib.sha3_256(b'.onion checksum' + pubkey + addr_version).digest()[:2]

                return checksum == expected_checksum

            return False

        except TypeError:
            return False
