#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Oniongroove.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import os
import logging

try:
    import yaml
except ImportError:
    print("Please install pyaml first!")
    raise ImportError

class OniongrooveInit:
    #
    # Initialization logic
    #

    def __init__(self, args):
        """
        Oniongroove class constructor.

        Setup instance configuration.

        Handles command-line parameters.

        :type  args: dict
        :param args: Instance arguments.
        """

        self.args = args
        self.data = []

        # Environment variable handling
        if 'ONIONGROOVE_CONFIG' in os.environ and os.environ['ONIONGROOVE_CONFIG'] != '':
            args.config = os.environ['ONIONGROOVE_CONFIG']

        # Config file handling
        if args.config is not None:
            if os.path.exists(args.config):
                with open(args.config, 'r') as config:
                    self.config = yaml.load(config, yaml.CLoader)
            else:
                raise FileNotFoundError(args.config)
        else:
            self.config = {}

        from .config import config, arti_template

        # Handle all other arguments
        for argument in config:
            value = getattr(args, argument)

            if value is not None and value != config[argument]['default']:
                self.config[argument] = value

        # Check for the Arti template
        if arti_template != None:
            if not os.path.exists(arti_template):
                raise FileNotFoundError(arti_template)

            self.arti_template = arti_template

    def initialize(self):
        """
        Oniongroove initialization procedures

        Initializes all Oniongroove subsystems, like logging, metrics and a Tor
        daemon instance.

        :rtype: bol
        :return: True if initialization is successful, False on error
        """

        # Initializes logging
        if self.initialize_logging() is False:
            return False

        #self.log('Oniongroove is initialized. Hit Ctrl-C to interrupt it.')

        return True
