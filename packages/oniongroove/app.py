#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Oniongroove.
#
# Copyright (C) 2024 The Tor Project, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import sys
import os
import signal

try:
    from .init      import OniongrooveInit
    from .config    import OniongrooveConfig
    from .logger    import OniongrooveLogger
    from .main      import OniongrooveMain
    from .teardown  import OniongrooveTeardown
    from .utils     import OniongrooveUtils
    from .arti      import OniongrooveArti
    from .openresty import OniongrooveOpenResty
except ImportError:
    exit (1)

class Oniongroove(
        # Inherit from subsystems
        OniongrooveInit,
        OniongrooveConfig,
        OniongrooveLogger,
        OniongrooveMain,
        OniongrooveTeardown,
        OniongrooveUtils,
        OniongrooveArti,
        OniongrooveOpenResty,
        ):
    """
    Oniongroove class to manage Onion Services
    """

def finish(status=0):
    """
    Stops Oniongroove

    :type  status: int
    :param status: Exit status code.
    """

    try:
        sys.exit(status)
    except SystemExit:
        os._exit(status)

def finish_handler(signal, frame):
    """
    Wrapper around finish() for handling system signals

    :type  signal: int
    :param signal: Signal number.

    :type  frame: object
    :param frame: Current stack frame.
    """

    print('Signal received, stopping Oniongroove..')

    finish(1)

def run(args):
    """
    Run Oniongroove from arguments

    :type  args: dict
    :param args: Instance arguments.
    """

    # Register signal handling
    #signal.signal(signal.SIGINT, finish_handler)
    signal.signal(signal.SIGTERM, finish_handler)

    # Exit status (shell convention means 0 is success, failure otherwise)
    status = 0

    # Dispatch
    try:
        groove = Oniongroove(args)

        if groove.initialize() is not False:
            status = 0 if groove.run() else 1
        else:
            status = 1

            print('Error: could not initialize')

    # Handle user interruption
    # See https://stackoverflow.com/questions/21120947/catching-keyboardinterrupt-in-python-during-program-shutdown
    except KeyboardInterrupt as e:
        groove.log('Stopping Oniongroove due to user request...')

    except FileNotFoundError as e:
        status = 1

        print('File not found: ' + str(e))

    except Exception as e:
        status = 1

        print(repr(e))

    finally:
        if 'groove' in locals():
            groove.close()

        finish(status)

def run_from_cmdline():
    """
    Run Oniongroove getting arguments from the command line.
    """

    from .config import cmdline

    run(cmdline())
