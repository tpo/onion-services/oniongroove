# Oniongroove Changelog

## v0.1.0 - Unreleased

* Full Vanguards support enabled by default on Arti.

## v0.0.2 - 2024-05-07

* Initial prototype. Check [tpo/onion-services/oniongroove#3][] for details.

[tpo/onion-services/oniongroove#3]: https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/issues/3

## v0.0.1 - 2022-08-04

* Initial specification.
