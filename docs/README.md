# Oniongroove: Deployment tool for the Onion Web

![Oniongroove Logo](assets/logo.jpg "Oniongroove")

[Oniongroove][] is planned to be a High Availability [Onion
Service](https://community.torproject.org/onion-services/) Deployment and
Monitoring system, giving groove to the
[Onionbalance](https://gitlab.torproject.org/tpo/core/onionbalance/) with
[Vanguards Addon](https://github.com/mikeperry-tor/vanguards) and many more
features.

Oniongroove acts as a proxy, load balancer or even a Content Delivery Network
(CDN) using the [Tor Network](https://torproject.org).

Oniongroove use case is similar to [Onionspray][] and [EOTK][], essentially
implementing a "reverse .onion proxy" between Tor Users and backend websites:

```mermaid
graph LR
  C[Client] -- .onion address via Tor Network --> O[Oniongroove CDN] -- Some network link --> U[Upstream site]
```

It can be used to:

1. Create Onion Services for any existing website.
2. Setting up "pure-onion" sites, i.e, those available only inside the Tor network.

[Oniongroove]: https://gitlab.torproject.org/tpo/onion-services/oniongroove
[Onionspray]: https://tpo.pages.torproject.net/onion-services/onionspray
[EOTK]: https://github.com/alecmuffett/eotk

Oniongroove aims to support both Onion Service [implementations][]: the old [C
Tor][] and the new [Arti][] as engines, making the migration to [Arti][]
easier.

[implementations]: https://onionservices.torproject.org/dev/implementations/
[C Tor]: https://gitlab.torproject.org/tpo/core/tor
[Arti]: https://arti.torproject.org

!!! note "Experimental stage"

    This software suite is still in the [experimental stage](prototype.md)!
    Check the [issue queue][] and [roadmap][] for details.

[issue queue]: https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/issues
[roadmap]: https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/milestones
