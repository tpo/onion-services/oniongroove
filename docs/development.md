# Development

## Release procedure

Release cycle workflow.

### Version update

Set the version number:

    ONIONGROOVE_VERSION=1.2.0

Update the version in some files, like:

    dch -i # debian/changelog
    $EDITOR packages/oniongroove/config.py
    $EDITOR docker-compose.yml
    $EDITOR setup.cfg

### Regenerate the manpage

    make manpage

### Register the changes

Update the ChangeLog:

    $EDITOR ChangeLog

Commit and tag:

    git diff # review
    git commit -a -m "Feat: Oniongroove $ONIONGROOVE_VERSION"
    git tag -s $ONIONGROOVE_VERSION -m "Oniongroove $ONIONGROOVE_VERSION"

Push changes and tags. Example:

    git push origin        && git push upstream
    git push origin --tags && git push upstream --tags

Once a tag is pushed, a [GitLab release][] is created.

[GitLab release]: https://docs.gitlab.com/ee/user/project/releases/

### Build packages

Build and then upload the Python package in the Test PyPi instance:

    make build-python-package
    make upload-python-test-package

Try the test package in a fresh virtual machine, eg:

    sudo apt-get install -y python3-pip tor
    pip install -i https://pypi.org/simple/ \
                --extra-index-url https://test.pypi.org/simple \
                --break-system-packages \
                oniongroove==$ONIONGROOVE_VERSION

Make sure to test after installation. If the the package works as expected,
upload it to PyPi:

    make upload-python-package

### Announcement

Announce the new release:

* Post a message to the [Tor Forum][], using the [onion-services-announce tag][].
* Send a message to the [tor-announce][] mailing list ONLY in special cases,
  like important security issues (severity `HIGH` or `CRITICAL`).

Template:

```
Subject: [RELEASE] Oniongroove [security] release $ONIONGROOVE_VERSION

Greetings,

We just released [Oniongroove][] $ONIONGROOVE_VERSION, an Onion Service
management tool.

[This release fixes a security issue. Please upgrade as soon as possible!]

[Oniongroove]: https://onionservices.torproject.org/apps/web/oniongroove

# ChangeLog

$CHANGELOG
```

[tor-announce]: https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-announce
[Tor Forum]: https://forum.torproject.org
[onion-services-announce tag]: https://forum.torproject.org/tag/onion-services-announce

## Notes

These are raw development notes made during the Oniongroove development.

### Implementation

Consider to plit architecture into implementors, and don't rely on specific choices:

* Onion Services:
    * Arti is an implementor for the Onion Service protocol.
    * Consider also to support C Tor as another Onion Service implementation.
      Involves more work, but may be useful for operators that want to migrate
      from solutions such as [Onionspray][] while Arti does not have Onion
      Service feature parity with C Tor.
* Proxy:
    * OpenResty is implementation for the rewriting proxy. NGINX maybe
      could be another, like Envoy etc.
* Deployment:
    * Same should happen with the deployment strategies: many flavors might
      be supported (but only one per provider at this point): Docker/Podman
      Compose, Nomad, Kubernetes, Ansible, Terraform etc. We may group
      between the container and non-container based.

[Onionspray]: https://gitlab.torproject.org/tpo/onion-services/onionspray/

## Arti

The notes below are questions and impressions when using [Arti][] for the first
time in an Onion Service project.

Appreciations:

* Acknowledge to Arti devs that the log messages are really nice! Example:

        2024-04-25T15:20:53Z  INFO tor_hsservice::publish::reactor: reuploading descriptor in 1h 5m time_period=TimePeriod { interval_num: 19838, length: IntegerMinutes { value: 1440 }, epoch_offset_in_sec: 43200 }

Wisthlist:

* Having a way to generate the Onion Service keys using the command line would
  be really useful for sysadmins.

Questions:

* SOCKS:
    * Is there a way to disable SOCKS listening when using Onion Services?
    * The config is `socks_port`, `socks_listen` or both?
* Proxy:
    * Why Onion Services are under the `proxy` command? This makes sense
      for clients that want to access .onion addresses, but maybe not for servers
      that are hosting Onion Services.
    * Option `proxy_ports` seems only to support IP addresses, not hostnames:
      `arti: error: read configuration: Config contents not as expected:
       Could not parse onion service target type "openresty:80"`
* Terminology:
    * Is `./arti hss onion-name` misleading? Shouldn't be `onion-addr`?

[Arti]: https://arti.torproject.org
