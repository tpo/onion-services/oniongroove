# Credits

## Index

[TOC]

## Assets

This repository uses the following images:

* [tor-logo1x.png](https://gitlab.torproject.org/tpo/web/community/-/blob/main/assets/static/images/tor-logo1x.png)
