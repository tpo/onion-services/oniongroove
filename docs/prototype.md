# Prototype

The current Oniongroove codebase is a [prototype][].

It's available for testing and tinkering.

## Characteristics

* Based on [Arti][] and [OpenResty][].
* The [full specification](specs.md) is not implemented yet.
* Lacks many configuration options both on the Onion Service and HTTPS sides.
* Only onionsites based on the HTTPS rewriting proxy are supported right now.
* Light configuration format: only a mapping between Onion Service addresses and
  it's DNS-based upstream domain names is needed in the proxy layer.
* On the fly self-signed certificate generation: HTTPS certificates are generated
  in the first TLS handshake if they're not already present in the file system.
  This allows testing how an onionsite would look like if there was already some
  automated way to get CA-validated certificates.

[Arti]: https://arti.torproject.org
[OpenResty]: https://openresty.org
[prototype]: https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/issues/3

## Requirements

This prototype currently requires [Docker][] and [Docker Compose][] properly
installed. The [provision][] script serves as an example in how to do that.

[provision]: https://gitlab.torproject.org/tpo/onion-services/oniongroove/-/blob/main/scripts/provision
[Docker]: https://docs.docker.com
[Docker Compose]: https://docs.docker.com/compose

## Installation

Get Oniongroove with it's submodules directly from the [repository][] using

    git clone --recursive https://gitlab.torproject.org/tpo/onion-services/oniongroove.git

Once installed, you enter the `oniongroove` folder an run commands from there.

[repository]: https://gitlab.torproject.org/tpo/onion-services/oniongroove

## Configuration

The prototype relies in the following configuration folder structure:

* `configs`:
    * `arti`: holds _compiled_ Arti configuration; do not edit those files
      directly.
    * `openresty`: holds _compiled_ OpenResty configuration; do not edit those
      files directly.
    * `oniongroove.yaml`: default and sample Oniongroove configuration; leave
      this file as a reference.
    * Other custom YAML configurations.

Begin by copying the sample configuration:

    cp configs/oniongroove.yaml configs/myprovider.yaml

Edit this file to suit your needs.

## Running

The main tool is the `oniongroove` script:

    ./oniongroove --help

To start Oniongroove, use the `start` action and pass a configuration file
path:

    ./oniongroove start configs/myprovider.yaml
